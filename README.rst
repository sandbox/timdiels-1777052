Current state: still coding on the first release.

Summary
-------

http://groups.drupal.org/node/252623


Installation
------------

Install as usual.


Configuration
-------------

Go to the configuration page to configure which content types should have
progress tracking.

Also don't forget to configure permissions.


Author
------

Tim Diels <tim@timdiels.be>
